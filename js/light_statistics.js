// $Id: light_statistics.js, 2012/01/31 17:46:43 MA

(function ($) {
	Drupal.behaviors.statistics = {
            attach: function (context, settings) {
                // Do an ajax callback to the given callback address
                $.ajax({
                        url: settings.statistics.callback,
                        data: {
                          checksum: settings.statistics.checksum,
                          data: settings.statistics.data
                        },
                        type: 'POST',
                        cache: false,
                        dataType: "html"
                });
            }
	};

})(jQuery);


