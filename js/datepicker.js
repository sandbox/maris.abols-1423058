// $Id: datepicker.js,v 1.1.2.1 2010/12/02 20:01:43 tcmug Exp $

(function ($) {
	Drupal.behaviors.ltcstatistics = {
            attach: function (context) {
                var date = new Date(); 
                var nowDate=date.getDate() + "." + (date.getMonth()+1) + "." + date.getFullYear();

                $( "#edit-created-min" ).datepicker({dateFormat: 'dd.mm.yy'});
                $( "#edit-created-max" ).datepicker({dateFormat: 'dd.mm.yy'});
                
                $( "#edit-created-min" ).datepicker().val(nowDate).trigger('change');
                $( "#edit-created-max" ).datepicker().val(nowDate).trigger('change');
            }
	};

})(jQuery);