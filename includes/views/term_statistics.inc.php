<?php

$view = new view;
$view->name = 'term_statistics';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'light_statistics_term';
$view->human_name = 'Kategoriju apmeklējuma statistika';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Taxonomy term statistics';
$handler->display->display_options['use_ajax'] = TRUE;
$handler->display->display_options['use_more_text'] = 'vairāk';
$handler->display->display_options['access']['type'] = 'role';
$handler->display->display_options['access']['role'] = array(
  3 => '3',
  5 => '5',
);
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['query_comment'] = FALSE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Attīrīt';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Kārtot pēc';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['expose']['items_per_page_options_all'] = 0;
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'name' => 'name',
  'name_1' => 'name_1',
  'created' => 'created',
  'visits' => 'visits',
  'name_2' => 'name_2',
);
$handler->display->display_options['style_options']['default'] = 'created';
$handler->display->display_options['style_options']['info'] = array(
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name_1' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'desc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'visits' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name_2' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
$handler->display->display_options['style_options']['override'] = 1;
$handler->display->display_options['style_options']['sticky'] = 0;
$handler->display->display_options['style_options']['empty_table'] = 0;
/* Field: Taxonomy term: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name']['alter']['external'] = 0;
$handler->display->display_options['fields']['name']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name']['alter']['html'] = 0;
$handler->display->display_options['fields']['name']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name']['hide_empty'] = 0;
$handler->display->display_options['fields']['name']['empty_zero'] = 0;
$handler->display->display_options['fields']['name']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['name']['link_to_taxonomy'] = 0;
/* Field: Role: Role name */
$handler->display->display_options['fields']['name_1']['id'] = 'name_1';
$handler->display->display_options['fields']['name_1']['table'] = 'role';
$handler->display->display_options['fields']['name_1']['field'] = 'name';
$handler->display->display_options['fields']['name_1']['label'] = 'Role';
$handler->display->display_options['fields']['name_1']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['external'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name_1']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name_1']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name_1']['alter']['html'] = 0;
$handler->display->display_options['fields']['name_1']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name_1']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name_1']['hide_empty'] = 0;
$handler->display->display_options['fields']['name_1']['empty_zero'] = 0;
$handler->display->display_options['fields']['name_1']['hide_alter_empty'] = 0;
/* Field: Statistics for term entities: Created date */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'light_statistics_term';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['label'] = 'Date';
$handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['created']['alter']['external'] = 0;
$handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['created']['alter']['trim'] = 0;
$handler->display->display_options['fields']['created']['alter']['html'] = 0;
$handler->display->display_options['fields']['created']['element_label_colon'] = 1;
$handler->display->display_options['fields']['created']['element_default_classes'] = 1;
$handler->display->display_options['fields']['created']['hide_empty'] = 0;
$handler->display->display_options['fields']['created']['empty_zero'] = 0;
$handler->display->display_options['fields']['created']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['created']['date_format'] = 'custom';
$handler->display->display_options['fields']['created']['custom_date_format'] = 'd.m.Y';
/* Field: Statistics for term entities: Entity visits */
$handler->display->display_options['fields']['visits']['id'] = 'visits';
$handler->display->display_options['fields']['visits']['table'] = 'light_statistics_term';
$handler->display->display_options['fields']['visits']['field'] = 'visits';
$handler->display->display_options['fields']['visits']['label'] = 'Visits';
$handler->display->display_options['fields']['visits']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['visits']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['visits']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['visits']['alter']['external'] = 0;
$handler->display->display_options['fields']['visits']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['visits']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['visits']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['visits']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['visits']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['visits']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['visits']['alter']['trim'] = 0;
$handler->display->display_options['fields']['visits']['alter']['html'] = 0;
$handler->display->display_options['fields']['visits']['element_label_colon'] = 1;
$handler->display->display_options['fields']['visits']['element_default_classes'] = 1;
$handler->display->display_options['fields']['visits']['hide_empty'] = 0;
$handler->display->display_options['fields']['visits']['empty_zero'] = 0;
$handler->display->display_options['fields']['visits']['hide_alter_empty'] = 0;
$handler->display->display_options['fields']['visits']['format_plural'] = 0;
/* Field: User: Name */
$handler->display->display_options['fields']['name_2']['id'] = 'name_2';
$handler->display->display_options['fields']['name_2']['table'] = 'users';
$handler->display->display_options['fields']['name_2']['field'] = 'name';
$handler->display->display_options['fields']['name_2']['label'] = 'Username';
$handler->display->display_options['fields']['name_2']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['external'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name_2']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name_2']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name_2']['alter']['html'] = 0;
$handler->display->display_options['fields']['name_2']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name_2']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name_2']['hide_empty'] = 0;
$handler->display->display_options['fields']['name_2']['empty_zero'] = 0;
$handler->display->display_options['fields']['name_2']['hide_alter_empty'] = 1;
$handler->display->display_options['fields']['name_2']['link_to_user'] = 1;
$handler->display->display_options['fields']['name_2']['overwrite_anonymous'] = 0;
$handler->display->display_options['fields']['name_2']['format_username'] = 1;
/* Field: Taxonomy vocabulary: Name */
$handler->display->display_options['fields']['name_3']['id'] = 'name_3';
$handler->display->display_options['fields']['name_3']['table'] = 'taxonomy_vocabulary';
$handler->display->display_options['fields']['name_3']['field'] = 'name';
$handler->display->display_options['fields']['name_3']['label'] = 'Vocabulary';
$handler->display->display_options['fields']['name_3']['alter']['alter_text'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['make_link'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['absolute'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['external'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['replace_spaces'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['trim_whitespace'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['nl2br'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['word_boundary'] = 1;
$handler->display->display_options['fields']['name_3']['alter']['ellipsis'] = 1;
$handler->display->display_options['fields']['name_3']['alter']['strip_tags'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['trim'] = 0;
$handler->display->display_options['fields']['name_3']['alter']['html'] = 0;
$handler->display->display_options['fields']['name_3']['element_label_colon'] = 1;
$handler->display->display_options['fields']['name_3']['element_default_classes'] = 1;
$handler->display->display_options['fields']['name_3']['hide_empty'] = 0;
$handler->display->display_options['fields']['name_3']['empty_zero'] = 0;
$handler->display->display_options['fields']['name_3']['hide_alter_empty'] = 1;
/* Filter criterion: Statistics for term entities: Created date */
$handler->display->display_options['filters']['created']['id'] = 'created';
$handler->display->display_options['filters']['created']['table'] = 'light_statistics_term';
$handler->display->display_options['filters']['created']['field'] = 'created';
$handler->display->display_options['filters']['created']['operator'] = 'between';
$handler->display->display_options['filters']['created']['exposed'] = TRUE;
$handler->display->display_options['filters']['created']['expose']['operator_id'] = 'created_op';
$handler->display->display_options['filters']['created']['expose']['label'] = 'Period';
$handler->display->display_options['filters']['created']['expose']['operator'] = 'created_op';
$handler->display->display_options['filters']['created']['expose']['identifier'] = 'created';
$handler->display->display_options['filters']['created']['expose']['multiple'] = FALSE;
/* Filter criterion: Role: Role name */
$handler->display->display_options['filters']['name']['id'] = 'name';
$handler->display->display_options['filters']['name']['table'] = 'role';
$handler->display->display_options['filters']['name']['field'] = 'name';
$handler->display->display_options['filters']['name']['value'] = array(
  1 => '1',
);
$handler->display->display_options['filters']['name']['exposed'] = TRUE;
$handler->display->display_options['filters']['name']['expose']['operator_id'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['label'] = 'Role';
$handler->display->display_options['filters']['name']['expose']['operator'] = 'name_op';
$handler->display->display_options['filters']['name']['expose']['identifier'] = 'name';
$handler->display->display_options['filters']['name']['expose']['reduce'] = 1;
$handler->display->display_options['filters']['name']['reduce_duplicates'] = 0;
/* Filter criterion: User: Name */
$handler->display->display_options['filters']['uid']['id'] = 'uid';
$handler->display->display_options['filters']['uid']['table'] = 'users';
$handler->display->display_options['filters']['uid']['field'] = 'uid';
$handler->display->display_options['filters']['uid']['value'] = '';
$handler->display->display_options['filters']['uid']['exposed'] = TRUE;
$handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['label'] = 'Username';
$handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
$handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
$handler->display->display_options['filters']['uid']['expose']['multiple'] = FALSE;
$handler->display->display_options['filters']['uid']['expose']['reduce'] = 0;
/* Filter criterion: Taxonomy term: Vocabulary */
$handler->display->display_options['filters']['vid']['id'] = 'vid';
$handler->display->display_options['filters']['vid']['table'] = 'taxonomy_term_data';
$handler->display->display_options['filters']['vid']['field'] = 'vid';
$handler->display->display_options['filters']['vid']['exposed'] = TRUE;
$handler->display->display_options['filters']['vid']['expose']['operator_id'] = 'vid_op';
$handler->display->display_options['filters']['vid']['expose']['label'] = 'Vocabulary';
$handler->display->display_options['filters']['vid']['expose']['operator'] = 'vid_op';
$handler->display->display_options['filters']['vid']['expose']['identifier'] = 'vid';
$handler->display->display_options['filters']['vid']['expose']['reduce'] = 0;

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'popular/taxonomy-term-statistics';
$handler->display->display_options['menu']['type'] = 'tab';
$handler->display->display_options['menu']['title'] = 'Taxonomy term statistics';
$handler->display->display_options['menu']['weight'] = '3';
$handler->display->display_options['menu']['name'] = 'main-menu';
$handler->display->display_options['menu']['context'] = 0;

/* Display: Data export */
$handler = $view->new_display('views_data_export', 'Data export', 'views_data_export_1');


?>