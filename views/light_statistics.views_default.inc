<?php

//if (module_exists('views')) {
  function light_statistics_views_default_views() {
    $views = Array();
    $files = file_scan_directory(drupal_get_path('module', 'light_statistics'). '/includes/views', '/.inc.php/');
    foreach ($files as $filepath => $file) {
      require $filepath;
      if (isset($view)) {
        $views[$view->name] = $view;
      }
    }
    return $views;
  }
//}