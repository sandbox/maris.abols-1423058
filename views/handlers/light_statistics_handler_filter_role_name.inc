<?php

class light_statistics_handler_filter_role_name extends views_handler_filter_user_roles {
  function get_value_options() {
    $this->value_options = user_roles(FALSE);
    unset($this->value_options[DRUPAL_AUTHENTICATED_RID]);
  }
  
  // the query method is responsible for actually running our exposed filter
  function query() {
    $this->ensure_my_table();
   
    // display unspecified and visible ones
    $this->query->add_where(1, "role.rid", $this->value["0"], "="); 
  }
}
