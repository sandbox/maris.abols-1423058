<?php

/**
 * @file
 * Views 2 hooks and callback registries.
 */



/**
* Implementation of hook_views_handlers().
*/
function light_statistics_views_handlers() {
  return array(
    'info' => array(
      'path' => drupal_get_path('module', 'light_statistics').'/views/handlers',
    ),
    'handlers' => array(
      'light_statistics_handler_filter_role_name' => array(
        'parent' => 'views_handler_filter_user_roles',
      ),
      'light_statistics_handler_filter_user_name' => array(
        'parent' => 'views_handler_filter_user_roles',
      ),
    ),
  );
}

/**
 * Implementation of hook_views_data().
 */
function light_statistics_views_data() {
  // node statistics view
  $data['light_statistics_node']['table']['group'] = t('Light statistics for node entities');

  $data['light_statistics_node']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Node statistics view'),
    'help' => t('Node statistics view'), 
  );
  
  $data['role']['table']['join'] = array(
    'light_statistics_node' => array(
      'left_field' => 'rid',
      'field' => 'rid',
    ),
    'light_statistics_term' => array(
      'left_field' => 'rid',
      'field' => 'rid',
    ),
  );
  
  $data['users']['table']['join'] = array(
    'light_statistics_node' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
    'light_statistics_term' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
  );
  
  $data['node']['table']['join'] = array(
    'light_statistics_node' => array(
      'left_field' => 'entity_id',
      'field' => 'nid',
    ),
  );
  
  // id
  $data['light_statistics_node']['id'] = array(
    'title' => t('Statistics ID'),
    'help' => t('Statistics primary key'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  // rid
  $data['light_statistics_node']['rid'] = array(
    'title' => t('Stats role ID'),
    'help' => t('Statistics role ID'), 
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'light_statistics_handler_filter_role_name',
      'numeric' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
 
  // uid
  $data['light_statistics_node']['uid'] = array(
    'title' => t('Stats user ID'),
    'help' => t('Statistics user ID'), 
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'light_statistics_handler_filter_user_name',
      'numeric' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  $data['light_statistics_node']['created'] = array(
    'title' => t('Created date'), 
    'help' => t('Statistics entry created date.'), 
    'field' => array(
      'handler' => 'views_handler_field_date', 
      'click sortable' => TRUE
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
    
  // Example numeric text field.
  $data['light_statistics_node']['visits'] = array(
    'title' => t('Entity visits'), 
    'help' => t('Entity visits.'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // -------------------------------------------------------
  
  // term statistics view
  $data['light_statistics_term']['table']['group'] = t('Statistics for term entities');

  $data['light_statistics_term']['table']['base'] = array(
    'field' => 'id',
    'title' => t('Taxonomy term statistics view'),
    'help' => t('Taxonomy term statistics view'), 
  );
    
  $data['taxonomy_term_data']['table']['join'] = array(
    'light_statistics_term' => array(
      'left_field' => 'entity_id',
      'field' => 'tid',
    ),
  );
  
  $data['taxonomy_vocabulary']['table']['join'] = array(
    'light_statistics_term' => array(
      'left_field' => 'vid',
      'field' => 'vid',
    ),
  );
  
  // id
  $data['light_statistics_term']['id'] = array(
    'title' => t('Term statistics ID'),
    'help' => t('Term statistics primary key'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  // rid
  $data['light_statistics_term']['rid'] = array(
    'title' => t('Stats role ID'),
    'help' => t('Statistics role ID'), 
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'light_statistics_handler_filter_role_name',
      'numeric' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  // uid
  $data['light_statistics_term']['uid'] = array(
    'title' => t('Stats user ID'),
    'help' => t('Statistics user ID'), 
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'light_statistics_handler_filter_user_name',
      'numeric' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  $data['light_statistics_term']['created'] = array(
    'title' => t('Created date'), 
    'help' => t('Statistics entry created date.'), 
    'field' => array(
      'handler' => 'views_handler_field_date', 
      'click sortable' => TRUE
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
    
  // numeric text field.
  $data['light_statistics_term']['visits'] = array(
    'title' => t('Entity visits'), 
    'help' => t('Entity visits.'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric', 
      'click sortable' => TRUE,
    ), 
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ), 
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  
  // need ROLE as base thingie
  $data['role']['table']['group']  = t('Role');

  $data['role']['table']['base'] = array(
    'field' => 'rid',
    'title' => t('Role'),
  );

  // rid
  $data['role']['rid'] = array(
    'title' => t('Rid'),
    'help' => t('Role primary key'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  // name
  $data['role']['name'] = array(
    'title' => t('Role name'),
    'help' => t('Role name'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'light_statistics_handler_filter_role_name',
      'numeric' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  // need USER as base thingie
  $data['user']['table']['group']  = t('User');

  $data['user']['table']['base'] = array(
    'field' => 'uid',
    'title' => t('User'),
  );

  // rid
  $data['user']['uid'] = array(
    'title' => t('Uid'),
    'help' => t('User primary key'), 
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  
  // name
  $data['user']['name'] = array(
    'title' => t('User name'),
    'help' => t('User name'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'light_statistics_handler_filter_user_name',
      'numeric' => TRUE,
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  
  return $data;
 
}
